---
layout: post
title: 'Getting Started with the Command-line'
date:   2014-09-19 21:12:00
categories: tutorials commandline
---

# Getting Started with the Command-line

### Why Do I Need The Terminal?

1. Often faster
* More powerful
* Cross-platform
  * when you learn to do a task from the command-line, you will probably be able to repeat it on any Unix-like system
  * Most online support will be provided via terminal commands
* Scripts/Aliases
  * When you do a task from the command-line frequently, you can create either a script or an alias which will execute the task from a single command
  * laptop.sh
  * upd - 'yaourt -Syua'
* Know thy system!

### Getting Started: Make Your Terminal More Friendly

1. Try another terminal emulator
  * Guake/Yakuake
  * Terminator
* Customize your terminal emulator
  * find a pleasant, easy to read monospaced font
    * DejaVu Sans Mono
    * make sure anti-aliasing is on!
    * balance amount of information on screen with legibility
  * find a color scheme with good contrast, yet something visually appealing
    * I prefer dark schemes
  * transparency?
    * with drop-down terminals, this can be handy if it allows you to read text that is behind the terminal emulator
* Auto-complete is your friend!
  * using tab to auto-complete is not only a shortcut to save time, but well confirm that what you've typed in is valid
  * sudo apt-get install term[tab]

### Your first 10 commands

1. whoami, hostname, pwd
  * ~ - is a shortcut for the current user's home directory
* Common command-line flags
  * -h, --help
  * -h, --human-readable
    * will convert quantity values to be easily readable (e.g. 1K 234M 2G)
  * -v, --version
    * for most applications -v will print out the current version number, though for some (e.g. rm or tar) it will enable verbose mode
  * -a, --all
  * -r, --recursive
    * when a command searches or modifies files (e.g. ls, rm), a recursive operation will operate not only on the current directory, but also child directories. *Warning:* this is sometimes -r and sometimes -R, so it's better to check the man page
* cd, ls
  * cd ..
  * ls -a, ls -l
* mv, cp, rm, mkdir, rmdir
* man
  * a very important command!
  * I'm only providing a brief intro to the commands I'm showing you, refer to the man-pages to learn about everything a command can do, and how to pass flags and options to it
* su, sudo
  * sudo whoami
  * su == su root
  * su is an application
  * su christopher (don't do this!)
  * su vs. sudo su
    * with "su", I need to know root's password
    * with "sudo su", I only need to know my user's password, and have permission to use sudo
* cat
* nano
* Your package manager!
  * Debian-based (Debian, Ubuntu, Linux Mint, Elementary OS, et al)
    * apt-get
  * arch
    * pacman
  * fedora
    * yum
  * openSUSE
    * zypper
* shutdown
  * sudo shutdown -h now

### Nerdier Commands

* uname -a
* grep and piping
* echo
* chown, chmod
* ps, top, htop
* wget
* tar, unzip, unrar
  * tar xvf
    * x: e<b>x</b>tract files from the archive
    * v: <b>v</b>erbose mode, print to console info
    * f: use archive <b>f</b>ile or device archive
