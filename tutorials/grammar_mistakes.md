###[Grammar Monster](www.grammar-monster.com)

* canvas vs. canvass
  * the former is heavy cloth, the latter to survey opinion or solicit votes
* comprise vs. compose
  * e.g. The cake comprises 4 slices
  * e.g. Four slices compose the cake
* curb vs. kerb
  * outside of North America, the edging of a pavement is spelled "kerb"
* disinterested vs. uninterested
  * the former means impartial, the latter not interested
* dived vs. dove
  * the former is UK convention, the latter US
* inquiry vs. enquiry
  * interchangeable, though the former is US convention, and in UK the former is usually associated with investigation, the latter with a question
* forego/forewent/foregone vs. forgo/forwent/forgone
  * the former mean "to precede", the latter "to do without (something)"
  * forego and forewent now uncommon
  * "a foregone conclusion"
  * "I will forgo food; she already forwent eating."
* hanged vs. hung
  * interchangeable; hung is generally preferable except in cases relating to death by hanging, in which case hanged is more appropriate
* if vs. whether
  * it gets complicated...
* to lay vs. to lie
  * the former means "to place in a horizontal position", the latter "to be in a horizontal position"
  * the past tense of "lie" is "lay"
  * the past tense of "lay" is "laid"
  * e.g. "the maids will lay the table for dinner"
  * e.g. "I recall when the bird laid her eggs"
* learned vs. learnt
  * as a past tense verb, the former is necessitated by US convention, the latter preferred by UK convention
    * both are pronounced with a single syllable
  * as an adjective, "learned" must be used universally, and is pronounced with two syllables
* license vs. licence
  * by US convention, both the noun and verb are spelled as the former
  * by UK convention, the former is the verb form, and the latter the noun
* loathe vs. loath
  * the former is a verb meaning "to hate", the latter an adjective meaning "unwilling"
* material vs. materiel
  * "material" is the matter from which a thing is made
  * "materiel" is the equipment or supplies in a military or commercial supply-chain management
* pendant vs. pendent
  * the former is a noun, the latter an adjective
* practice vs. practise
  * by UK convention, the former is the noun and the latter the verb
* precedence vs. precedent
  * the former is of ranking, the latter a former example used to guide a decision
* shall vs. will
  * it gets complicated...
* whether vs. weather vs. wether
  * a "wether" is a castrated ram (male sheep)
* while vs. whilst
  * by US convention "whilst" is quaint and dated, and perhaps pretentious-sounding
  * by UK convention they are interchangeable, with the former perhaps preferred
* who vs. whom
  * the former is the subject of a verb, the latter not
  * e.g. "who paid for it?" ("who" is the subject of the verb "to pay")
  * e.g. "she is the one to whom I spoke" ("she" is the subject of "is" and "I" is the subject of "spoke")
  * when in doubt, use "who", as it is more common, and *almost* acceptable in the latter case
