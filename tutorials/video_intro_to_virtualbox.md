# Introduction to VirtualBox

1. What is virtualization?
  1. To put it simply, virtualization allows you to run one operating system within another, like the dreams in *Inception*.
  2. With virtualization, you can trick an operating system into thinking that it's running on its own computer hardware, when it's really running completely within an application
  3. Host and guest relationship
    1. The *guest* operating system runs within an application (the virtualizer) on the *host* operating system.
    2. In theory, the *guest* operating system will have no access to the *host*. As far as the *guest* can tell, it is running on its own computer hardware.
2. What you need to set up a virtual machine
  1. Access to a working host system with available resources
  2. An installed virtualizer (e.g. VirtualBox)
  3. A virtual machine
    1. This will be created with your virtualizer
    2. This will be limited by the system resources of the host
  4. A virtual disk image
    1. Again, created by your virtualizer
    2. This will require free storage space on your host
  5. Installation media for the guest operating system
